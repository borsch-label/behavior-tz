/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SpiderFilterService } from './spider-filter.service';

describe('Service: SpiderFilter', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SpiderFilterService]
    });
  });

  it('should ...', inject([SpiderFilterService], (service: SpiderFilterService) => {
    expect(service).toBeTruthy();
  }));
});

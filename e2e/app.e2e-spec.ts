import { BehaviorPage } from './app.po';

describe('behavior App', function() {
  let page: BehaviorPage;

  beforeEach(() => {
    page = new BehaviorPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
